import React, { Component } from "react";
import "./form.css";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      type: "Food",
      info: "",
    };
  }
  handleInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    //console.log("hii");

    e.preventDefault();
    this.props.handleSubmit(this.state);
  };

  render() {
    return (
      <form autoComplete="off" onSubmit={this.handleSubmit} className="form">
        <h1 className="title">Expense Manager</h1>
        <br></br>
        <label className="li">
          {" "}
          Enter your TotalExpense:
          <input
            type="number"
            name="value"
            // value={this.state.value}
            placeholder=" Rs."
            onChange={this.handleInput}
            className="inputfield"
            required
          ></input>
        </label>
        <br />
        <label className="li">
          {" "}
          Add Discription:
          <input
            type="text"
            name="info"
            // value={this.state.value}
            placeholder=" write something..."
            onChange={this.handleInput}
            className="inputfield"
            required
          ></input>
        </label>

        <label htmlFor="options" className="li">
          Pick any one cetegory:
          <br></br>
          <div className="option">
            <select
              name="type"
              id="category"
              value={this.state.type}
              onChange={this.handleInput}
            >
              <option className="options" value="Food">
                Food
              </option>
              <option className="options" value="Home">
                Home
              </option>
              <option className="options" value="Shopping">
                Shopping
              </option>
            </select>
          </div>
        </label>
        <br></br>
        <button type="submit">Add Expense</button>
      </form>
    );
  }
}
export default Form;
