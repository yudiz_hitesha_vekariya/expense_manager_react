import React, { Component } from "react";
import Form from "./Form";
import "./form.css";

class Expenseadd extends Component {
  state = {
    list: [],
  };

  totalExpense = (target) => {
    return this.state.list
      .filter((item) => {
        return item.type === target;
      })
      .reduce((acc, cur) => {
        return (acc += Number(cur.value));
      }, 0);
  };
  handleSubmit = (data) => {
    var list = this.state.list;
    list.push(data);
    this.setState({ list });
  };

  handleDelete = (e) => {
    //console.log(e);
    var list = this.state.list;
    list.splice(e, 1);
    this.setState({ list });
  };

  render() {
    //console.log(this.state.list);

    return (
      <div className="main">
        <Form handleSubmit={this.handleSubmit} />

        <table className="form">
          <div>
            <h4>total:{this.state.list.length}</h4>
          </div>
          <tbody>
            {this.state.list.map((item, index) => {
              return (
                <tr key={index} className="tr">
                  <td className="tdh">{item.type}</td>
                  <td className="td"> Rs: {item.value}</td>
                  <td className="td"> {item.info}</td>
                  <td>
                    <button
                      className="btn"
                      onClick={() => this.handleDelete(index)}
                      dataset={index}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>

        <div className="form" id="f1">
          <div className="add">
            <h4>Food_Expense:-</h4>
            <p>{this.totalExpense("Food")}</p>
          </div>
          <div className="add">
            <h4>Home_Expense:-</h4>
            <p>{this.totalExpense("Home")}</p>
          </div>
          <div className="add">
            <h4>Shopping_Expense:-</h4>
            <p>{this.totalExpense("Shopping")}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Expenseadd;
